//
//  ViewController.m
//  UIAlertView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/4/14.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toClick:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert View" message:@"請選擇" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"儲存", @"載入", nil];
    
    [alertView show];
    
}
/*
    clickedButtonAtIndex 可由buttonIndex取得按下按鈕後得到的注標值
    因此可由注標值決定執行哪些程式的判斷基準
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%ld", (long)buttonIndex);
}


@end
